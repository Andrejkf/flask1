##### Flask Web App in GitLab pages

In GitLab Pages says:

![image-20210512200727287](/home/ai/.config/Typora/typora-user-images/image-20210512200727287.png)

For that reason Static Website was done using python **flask_frozen** .



###### Python Click project

* https://click.palletsprojects.com/en/8.0.x/

Click is a Python package for creating beautiful command line interfaces in a composable way with as little code as necessary.



###### Custom CLI(Command Line Interface) commands

https://flask.palletsprojects.com/en/1.1.x/cli/#custom-commands

Example:

```python
import click
from flask import Flask

app = Flask(__name__)

@app.cli.command("create-user")
@click.argument("name")
def create_user(name):
    ...
```



###### Hello World local node

1. Create `app1.py` file .

2.  In terminal export variables.

    ```bash
    export FLASK_APP=app1
    ```

3. Then run it in development mode:

    ```bash
    export FLASK_ENV=development
    ```

4. Flask run:

    ```bash
    flask run
    ```

    

###### Hello World GitLab pages

Use python `flask_frozen`  to develop static website to be hosted in GitLab Pages.

1. Create `app1.py` file .
2. Create `.gitignore` file.
3. Create `.gitlab-ci.yml` file .
4. Create file `requirements.txt` with list of required dependencies.
5. Commit changes and push to remote repository in GitLab.

```bash
git add .
git commit -m "First commit"
git push --set-upstream git@gitlab.com:GITLAB-USER/REPO-NAME-HERE.git master
```

