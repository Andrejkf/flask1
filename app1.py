from flask import Flask
from flask_frozen import Freezer

# 1. Instantiate class
app = Flask(__name__) #Create a Flask application Instancewith the name "app"
freezer = Freezer(app)

# 2. Define Paths for Web pages
app.config['FREEZER_BASE_URL'] = 'https://andrejkf.gitlab.io/' # Set up URl
app.config['FREEZER_DESTINATION'] = 'public' # Set folder to paste files

# 3. Define Custom functions
@app.cli.command("renderCustomCommand") # To define custom command "freeze" . Command Line Interface in a composable way with as little code as necessary
def renderCustomCommand(): #custom command definiton to be able to upload files to public folder in the Website
    freezer.freeze()

# 4. Path Web requests/Responses
@app.route('/') # Handle incoming web requests and send responses to the end user (decorator that turns a regular Python function into a Flask view function)
def doHello():
    return "Hello function text7."